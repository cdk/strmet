package main

import (
	"flag"
	"fmt"
	"github.com/cameronkennedy/strmet"
	"seqio"
)

var t *seqio.FATag = new(seqio.FATag)
var VERSION string = "0.001"
var base string = "AAA"
var base2 string = "CCC"
var kmer int = 3
var file = "/dev/stdin"

func flaginit() {
	flag.Usage = func() {
		fmt.Println("kd -b \"baseRPT\" <file>")
	}
	flag.StringVar(&base, "b", "AAA", "set origin to be repeat AAA")
	flag.StringVar(&base2,"b2","CCC","base 2 origin")
	flag.IntVar(&kmer, "k", 3, "kmer size for string metrics")
	flag.StringVar(&file,"f","/dev/stdin","input file")
}
func main() {
	flaginit()
	flag.Parse()
	args := flag.Args()
	if len(args) == 0 {
		flag.Usage()
	}
	fasta := seqio.GetTaggedChannel(file,"fa","")
	for t:=range fasta {
		go func(t seqio.Tagged){
		id:=string(t.(*seqio.FATag).Id)
		kd:=strmet.KennedySim1(base,string(t.Tag()),kmer)
		kd2:=strmet.KennedySim1(base2,string(t.Tag()),kmer)
		fmt.Printf("%v\t%v\t%v\n",id,kd,kd2)
		}(t)
	}
	d:=<-seqio.InputDone
	fmt.Println("d:",d)
	/*
	if b, err := ioutil.ReadFile(args[0]); err == nil {
		buf = bytes.NewBuffer(b)
	}
	for line, err := buf.ReadBytes('\n'); err == nil; line, err = buf.ReadBytes('\n') {
		linestr := string(bytes.TrimSpace(line))
		ksim := strmet.KennedySim1(base, linestr, kmer)
		fmt.Printf("%v\t%v\t%v\n", base, linestr, ksim)
	}
	*/
}
