clear
reset
set terminal svg
set border 3
set grid
set xrange [0:20]
set yrange [0:1500]
# Add a vertical dotted line at x=0 to show centre (mean) of distribution.
set yzeroaxis
set ylabel "count"
set xlabel "Distance"
# Each bar is half the (visual) width of its x-range.
set boxwidth 0.05 absolute
set style fill solid 1.0 noborder
set title "repeat distance vs count"
bin_width = 0.5;

bin_number(x) = floor(x/bin_width)

rounded(x) = bin_width * ( bin_number(x) + 0.5 )
plot "ants.gnuplot"  using (rounded($3)):(1) title 'Ants' smooth frequency with lp,\
"silkworm.gnuplot" using (rounded($3)):(1) title 'Silkworm' smooth frequency with lp,\
"spidermite.gnuplot" u (rounded($3)):(1) title 'Spidermite' smooth freq w lp,\
"dmel.gnuplot" u (rounded($3)):(1) t 'D.Mel' smooth freq w lp

