package main

import (
	"bytes"
	"flag"
	"fmt"
	"github.com/cameronkennedy/strmet"
	"io/ioutil"
)

var VERSION string = "0.001"
var base string = "AAA"
var kmer int = 3

func flaginit() {
	flag.Usage = func() {
		fmt.Println("kd -b \"baseRPT\" <file>")
	}
	flag.StringVar(&base, "b", "AAA", "set origin to be repeat AAA")
	flag.IntVar(&kmer, "k", 3, "kmer size for string metrics")
}
func main() {
	flaginit()
	flag.Parse()
	args := flag.Args()
	var buf *bytes.Buffer
	if len(args) == 0 {
		flag.Usage()
	}
	if b, err := ioutil.ReadFile(args[0]); err == nil {
		buf = bytes.NewBuffer(b)
	}
	for line, err := buf.ReadBytes('\n'); err == nil; line, err = buf.ReadBytes('\n') {
		linestr := string(bytes.TrimSpace(line))
		ksim := strmet.KennedySim1(base, linestr, kmer)
		fmt.Println("a:", base, "b:", linestr, "ksim:", ksim)
	}
}
