package main

import (
	"flag"
	"fmt"
	"github.com/cameronkennedy/strmet"
	"seqio"
)

var t *seqio.FATag = new(seqio.FATag)
var VERSION string = "0.001"
var kmer int = 3
var file = "/dev/stdin"
var file2= "/dev/stdin"
func flaginit() {
	flag.Usage = func() {
		fmt.Println("kd -b \"baseRPT\" <file>")
	}
	flag.IntVar(&kmer, "k", 3, "kmer size for string metrics")
	flag.StringVar(&file,"f","/dev/stdin","input file")
	flag.StringVar(&file2,"f2","/dev/stdin","input file2")
}
func main() {
	flaginit()
	flag.Parse()
	args := flag.Args()
	if len(args) != 0 {
		flag.Usage()
	}
	tags := make([]string,0)
	tags2 := make([]string,0)
	ksm := make([]float64,0)
	fasta := seqio.GetTaggedChannel(file,"fa","")
	fasta2 := seqio.GetTaggedChannel(file,"fa","")
	for t:=range fasta {
		tags = append(tags,string(t.Tag()))
	}
	for t:=range fasta2 {
		tags2 = append(tags2,string(t.Tag()))
	}
	data:=make([][]float64,len(tags))
	for i:=0;i<len(tags);i++{
		data[i]=make([]float64,len(tags2))
	}	
	for i:=0;i<len(tags);i++{
		min := float64(len(tags)*len(tags2))
		for j:=i;j<len(tags2);j++{
			d:=strmet.KennedySim1(tags[i],tags2[j],kmer)
			if d < min {
				min = d
			}
			data[i][j]=d
		}
		ksm=append(ksm,min)	
	}
	fmt.Printf("\t")
	for t:=range tags {
		fmt.Printf("%v\t",t)
	}
	fmt.Printf("\n")
	for i:=0;i<len(tags);i++{
		fmt.Printf("%v\t",tags[i])
		for j:=0;j<len(tags);j++{
			fmt.Printf("%v\t",data[i][j])
		}
		fmt.Printf("\n")
	}
	fmt.Printf("\n\n")
	total:=float64(0)
	for _,v:=range ksm {
		total+=v
	}
	fmt.Println("ksm:",total)

}
