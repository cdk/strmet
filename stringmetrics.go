package strmet

import (
	"bytes"
	"math"
	"strings"

	"bitbucket.org/cdk/sets"
)

func Min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func Max(a, b int) int {
	if a < b {
		return b
	}
	return a
}
func SameLength(a, b string) (string, string) {
	aLen := len(a)
	bLen := len(b)
	if aLen == bLen {
		return a, b
	}
	if Min(aLen, bLen) == aLen {
		//a is shorter than b
		times := bLen / aLen
		remainder := bLen % aLen
		if times > 0 {
			a = strings.Repeat(a, times)
		}
		if remainder > 0 {
			a = a + a[0:remainder]
		}
	} else {
		times := aLen / bLen
		remainder := aLen % bLen
		if times > 0 {
			b = strings.Repeat(b, times)
		}
		if remainder > 0 {
			b = b + b[0:remainder]
		}
	}
	if len(a) == len(b) {
		return a, b
	}
	panic("unable to make a and b the same length" + a + " " + b)
}
func MinFloat(a ...float64) (min float64) {
	min = a[0]
	for _, i := range a[1:] {
		if i <= min {
			min = i
		}

	}
	return min

}
func MaxFloat(a ...float64) (max float64) {
	max = a[0]
	for _, i := range a[1:] {
		if i > max {
			max = i
		}
	}
	return max

}
func MinOf(a ...int) (min int) {
	min = a[0]
	for _, i := range a[1:] {
		if i <= min {
			min = i
		}

	}
	return min
}
func MaxOf(a ...int) (max int) {
	max = a[0]
	for _, i := range a[1:] {
		if i > max {
			max = i
		}
	}
	return max
}
func Bimers(a string) []string {
	table := make(map[string]bool)
	//fmt.Println("A:", a)
	for i := 0; i+1 < len(a); i++ {
		table[a[i:i+2]] = true
	}
	ret := make([]string, len(table))
	i := 0
	for k, _ := range table {
		ret[i] = k
		i++
	}
	return ret
}
func Kmers(k int, a string) []string {
	table := make(map[string]bool)
	//fmt.Println("A:", a, "k:", k)
	for i := 0; i+k-1 < len(a); i++ {
		table[a[i:i+k]] = true
	}
	ret := make([]string, len(table))
	i := 0
	for k, _ := range table {
		ret[i] = k
		i++
	}
	return ret
}
func KmerCounts(k int, a string) map[string]int {
	ret := make(map[string]int)
	for i := 0; i+k-1 < len(a); i++ {
		ret[a[i:i+k]] += 1
	}
	return ret
}

func Reverse(a string) string {
	n := len(a)
	ret := []byte(a)
	for i := 0; i < n/2; i++ {
		ret[i], ret[n-1-i] = ret[n-1-i], ret[i]
	}
	return string(ret)
}
func correctReverse(a, b string) string {
	c := Reverse(b)
	lcstr, _ := LongestCommonSubstring(a, c)
	l := len(lcstr)
	i := strings.Index(c, lcstr)
	ss := c[i : i+l]
	sr := Reverse(ss)
	ret := c[0:i] + sr + c[i+l:]
	//fmt.Printf("%v\n%v\n%v\n", a, b, Reverse(ret))
	return Reverse(ret)
}
func LongestCommonSubstring(first, second string) (string, int) {
	maxLen := 0
	var indx int
	fl := len(first) + 0
	sl := len(second) + 0
	table := make([][]int, fl)
	for i := 0; i < fl; i++ {
		table[i] = make([]int, sl)
	}
	for i := 0; i < fl; i++ {
		for j := 0; j < sl; j++ {
			if first[i] == second[j] {
				if i == 0 || j == 0 {
					table[i][j] = 1
				} else {
					table[i][j] = table[i-1][j-1] + 1
				}
				if table[i][j] > maxLen {
					maxLen = table[i][j]
					indx = 1 + j - maxLen
				}
			}
		}
	}
	lcstr := second[indx : indx+maxLen]
	return lcstr, maxLen
}
func LongestCommonSubsequenceRecursive(a, b string) string {
	aLen := len(a)
	bLen := len(b)
	if aLen == 0 || bLen == 0 {
		return ""
	} else if a[aLen-1] == b[bLen-1] {
		return LongestCommonSubsequenceRecursive(a[:aLen-1], b[:bLen-1]) + string(a[aLen-1])
	}
	x := LongestCommonSubsequenceRecursive(a, b[:bLen-1])
	y := LongestCommonSubsequenceRecursive(a[:aLen-1], b)
	if len(x) > len(y) {
		return x
	}
	return y
}

func LongestCommonSubsequence(a, b string) (string, int) {
	aLen := len(a) + 1
	bLen := len(b) + 1
	lengths := make([][]int, aLen)
	for i := 0; i < aLen; i++ {
		lengths[i] = make([]int, bLen)
	}
	for i := 0; i < aLen-1; i++ {
		for j := 0; j < bLen-1; j++ {
			if a[i] == b[j] {
				lengths[i+1][j+1] = lengths[i][j] + 1
			} else {
				lengths[i+1][j+1] = Max(lengths[i+1][j], lengths[i][j+1])
			}
		}
	}
	buf := new(bytes.Buffer)
	i := aLen - 1
	j := bLen - 1
	for i != 0 && j != 0 {
		switch lengths[i][j] {
		case lengths[i-1][j]:
			i--
		case lengths[i][j-1]:
			j--
		default:
			if a[i-1] == b[j-1] {
				buf.WriteString(string(a[i-1]))
			}
			i--
			j--

		}
	}
	return Reverse(buf.String()), buf.Len()
}

//based on RobYoung's
//github.com/robyoung/go.stringmetrics
func Levenshtein(a, b string) int {
	aLen, bLen := len(a), len(b)
	if bLen > aLen {
		a, b = b, a
		bLen, aLen = aLen, bLen
	}
	current := make([]int, aLen+1)
	previous := make([]int, aLen+1)
	var insert, remove, change int

	for i := 1; i <= aLen; i++ {
		copy(previous, current)
		for j := 0; j <= aLen; j++ {
			current[j] = 0
		}
		current[0] = i
		for j := 1; j <= bLen; j++ {
			if a[i-1] == b[j-1] {
				current[j] = previous[j-1]
			} else {
				insert = previous[j] + 1
				remove = current[j-1] + 1
				change = previous[j-1] + 1
				current[j] = MinOf(insert, remove, change)
			}
		}
	}

	return current[bLen]
}
func LevenshteinKennedy(a, b string) int {
	k := 3 //intergrated reverse cost
	j := 2 //complete reverse cost
	brc := correctReverse(a, b)
	//fmt.Printf("a:%v\tb:%v\tcrev:%v\n", a, b, brc)
	//d := Min(Levenshtein(a, b), Min(j+Levenshtein(a, Reverse(b)), k+Levenshtein(a, brc)))
	d := MinOf(DemeraulLevenshtein(a, b), j+DemeraulLevenshtein(a, Reverse(b)), k+DemeraulLevenshtein(a, brc))
	return d
}

//based on python version by Michael Homer
func DemeraulLevenshtein(a, b string) int {
	aLen, bLen := len(a), len(b)
	if bLen > aLen {
		a, b = b, a
		aLen, bLen = bLen, aLen
	}
	current := make([]int, aLen+1)
	previous := make([]int, aLen+1)
	twoago := make([]int, aLen+1)
	var insert, remove, change, cost int

	for i := 1; i <= aLen; i++ {
		copy(twoago, previous)
		copy(previous, current)
		for j := 0; j <= aLen; j++ {
			current[j] = 0
		}
		current[0] = i
		for j := 1; j <= bLen; j++ {
			if a[i-1] == b[j-1] {
				current[j] = previous[j-1]
				cost = 0
			} else {
				cost = 1
				insert = previous[j] + cost
				remove = current[j-1] + cost
				change = previous[j-1] + cost
				current[j] = MinOf(insert, remove, change)
			}
			if i > 1 && j > 1 && a[i-1] == b[j-2] && a[i-2] == b[j-1] && a[i-1] != b[j-1] {
				current[j] = Min(current[j], cost+twoago[j-2])
				//fmt.Println("transposing !")
			} else {
				//fmt.Println("not transposing!")
			}
		}
	}

	return current[bLen]
}

//Based on Rob Youngs Jaro implementation
//github.com/RobYoung/go.stringmetrics
func Jaro(a, b string) float64 {
	n, m := len(a), len(b)
	if n == 0 || m == 0 {
		if n == 0 && m == 0 {
			return 1.0
		}
		return 0.0
	}

	if n > m {
		a, b = b, a
		n, m = m, n
	}

	search_range := (n / 2) - 1
	if search_range < 0 {
		search_range = 0
	}

	idx1 := make([]bool, n)
	idx2 := make([]bool, m)

	match := 0
	for i := 0; i < n; i++ {
		start := Max(0, i-search_range)
		end := Min(i+search_range+1, m)
		for j := start; j < end; j++ {
			if !idx2[j] && a[i] == b[j] {
				idx1[i] = true
				idx2[j] = true
				match++
				break
			}
		}
	}
	if match == 0 {
		return 0.0
	}

	var i, j, trans int
	for i, j, trans = 0, 0, 0; i < n; i++ {
		if idx1[i] {
			for !idx2[j] {
				j++
			}
			if a[i] != b[j] {
				trans++
			}
			j++
		}
	}

	matchf := float64(match)
	return (matchf/float64(n) + matchf/float64(m) + float64(match-trans/2)/matchf) / 3.0
}

//from wikipedia link to algorithms
func JaroWinkler(a, b string) float64 {
	weight := Jaro(a, b)
	if weight > 0.7 {
		i, j := 0, MinOf(len(a), len(b), 4) //min(min(len(a), len(b)), 4)
		for ; i < j; i++ {
			if a[i] != b[i] {
				break
			}
		}
		weight += float64(i) * 0.1 * (1.0 - weight)
	}

	return weight
}

//for two strings return number of changes needed to make a==b
func Hamming(a, b string) int {
	aLen := len(a)
	bLen := len(b)
	if bLen > aLen {
		a, b = b, a
		aLen, bLen = bLen, aLen
	}
	cost := aLen - bLen //extra character cost
	for i := 0; i < bLen; i++ {
		if a[i] != b[i] {
			cost++
		}
	}
	return cost
}
func Dice(a, b string, kmer int) float64 {
	A := sets.NewStringSet()
	B := sets.NewStringSet()
	I := sets.NewStringSet()
	sets.AddArray(A, Kmers(kmer, a))
	sets.AddArray(B, Kmers(kmer, b))
	I.Union(A)
	I.Intersection(B)

	return 2 * float64(I.Size()) / float64(A.Size()+B.Size())
}

//twice the intersection over the union
func Jaccard(a, b string, kmer int) float64 {
	A := sets.NewStringSet()
	B := sets.NewStringSet()
	I := sets.NewStringSet()
	U := sets.NewStringSet()
	sets.AddArray(A, Kmers(kmer, a))
	sets.AddArray(B, Kmers(kmer, b))
	I.Union(A)
	I.Intersection(B)
	U.Union(A)
	U.Union(B)
	jaccard := float64(I.Size()) / float64(U.Size())
	return jaccard
}

//intersection divided by (-intersection + A.size + B.size) // Tanimoto version of jaccard
func Tanimoto(a, b string, kmer int) float64 {
	A := sets.NewStringSet()
	B := sets.NewStringSet()
	I := sets.NewStringSet()
	sets.AddArray(A, Kmers(kmer, a))
	sets.AddArray(B, Kmers(kmer, b))
	I.Union(A)
	I.Intersection(B)
	tanimoto := float64(I.Size()) / float64(A.Size()+B.Size()-I.Size())
	return tanimoto
}
func TanimotoDistance(a, b string, kmer int) float64 {
	return 1.0 - Tanimoto(a, b, kmer)
}

//Tversky takes two strings, protoype and variant weights, and kmer size
func Tversky(a, b string, p, v float64, kmer int) float64 {
	A := sets.NewStringSet()
	B := sets.NewStringSet()
	I := sets.NewStringSet()
	P := sets.NewStringSet()
	V := sets.NewStringSet()
	sets.AddArray(A, Kmers(kmer, a))
	sets.AddArray(B, Kmers(kmer, b))
	I.Union(A)
	I.Intersection(B)
	P.Union(A)
	P.Difference(B)
	V.Union(B)
	V.Difference(A)
	intersection := float64(I.Size())
	prototype := p * float64(P.Size())
	variant := v * float64(V.Size())
	tversky := intersection / (intersection + prototype + variant)
	return tversky
}

//Overlap coefficient is intersection/min(|A|,|B|)
func OverlapCoeffecient(a, b string, kmer int) float64 {
	A := sets.NewStringSet()
	B := sets.NewStringSet()
	I := sets.NewStringSet()
	sets.AddArray(A, Kmers(kmer, a))
	sets.AddArray(B, Kmers(kmer, b))
	I.Union(A)
	I.Intersection(B)
	return float64(I.Size()) / float64(Min(A.Size(), B.Size()))
}

//cosine similarity
func CosineSimilarity(a, b string, kmer int) float64 {
	A := sets.NewStringSet()
	B := sets.NewStringSet()
	I := sets.NewStringSet()
	sets.AddArray(A, Kmers(kmer, a))
	sets.AddArray(B, Kmers(kmer, b))
	I.Union(A)
	I.Intersection(B)
	sim := float64(I.Size()) / math.Sqrt(float64(A.Size()*B.Size()))
	return sim
}

// distance based on Cosine Angle Similarity
//dist=sqrt( 2(1-CosineSimilarity(a,b)))
//I think...
func RawDistance(a, b string, kmer int) float64 {
	return math.Sqrt(2 * (1 - CosineSimilarity(a, b, kmer)))
}

//distance based on kmer content
func EuclideanDistance(a, b string, kmer int) float64 {
	var ret float64
	if len(a) == 1 {
		a = strings.Repeat(a, 2)
	}
	if len(b) == 1 {
		b = strings.Repeat(b, 2)
	}

	amers := KmerCounts(kmer, a[:]+a[0:2])
	bmers := KmerCounts(kmer, b[:]+b[0:2])
	kmers := sets.NewStringSet()
	for k, _ := range amers {
		kmers.Add(k)
	}
	for k, _ := range bmers {
		kmers.Add(k)
	}
	for _, val := range kmers.Contents() {
		var aval, bval int
		if val, exists := amers[val]; exists {
			aval = val
		}
		if val, exists := bmers[val]; exists {
			bval = val
		}
		ret += float64((aval - bval) * (aval - bval))
	}
	ret = math.Sqrt(ret)
	return ret
}

/*
	completness+E(a,b)*(1-Sim)
	E is KlevDemeraul
	Sim is
	 1/4 * (
	Jaccard(a,b)
+ 	(LongestCommonSubseq(a,b)/minLength(a,b))
+	(LongestCommonSubstring(a,b)/minLength(a,b) )
+ 	((minLength(a,b)-EditDistance)/minLength(a,b))
*/
func KennedySim1(a, b string, kmer int) float64 {
	bases := []string{"A", "T", "G", "C"}
	var acmplete float64 = 0
	var bcmplete float64 = 0
	for _, base := range bases {
		if strings.Contains(a, base) {
			acmplete++
		}
		if strings.Contains(b, base) {
			bcmplete++
		}
	}
	completeRatio := acmplete / bcmplete
	//add kmer bases to strings for set comparisons
	if len(a) == 1 {
		a = strings.Repeat(a, 2)
	}
	if len(b) == 1 {
		b = strings.Repeat(b, 2)
	}
	//amod := a[:] + a[0:kmer-1]
	//bmod := b[:] + b[0:kmer-1]
	amod, bmod := SameLength(a, b)
	/*
		leva := amod
		levb := bmod
		leva2 := amod
		levb2 := bmod
		if len(amod) > len(bmod) {
			btimes := len(amod) / len(bmod)
			if btimes > 1 {
				levb = strings.Repeat(bmod, btimes)
				levb2 = strings.Repeat(bmod, btimes+1)
			}
		} else if len(bmod) > len(amod) {
			atimes := len(bmod) / len(amod)
			if atimes > 1 {
				leva = strings.Repeat(amod, atimes)
				leva2 = strings.Repeat(amod, atimes+1)
			}
		}
	*/
	jaccard := Jaccard(amod, bmod, kmer)
	_, lcsseqlen := LongestCommonSubsequence(amod, bmod)
	_, lcsstrlen := LongestCommonSubstring(amod, bmod)
	minL := Min(len(amod), len(bmod))
	//edist := MinOf(LevenshteinKennedy(amod, bmod), LevenshteinKennedy(leva, levb), LevenshteinKennedy(leva2, levb2))
	/*
		This should be done so that a and be are aligned to the maximum subsequence location before calculating the LKD
	*/
	edist := LevenshteinKennedy(amod, bmod)
	patA := float64(strings.Count(a, "A")+strings.Count(a, "T")) / float64(len(a))
	patB := float64(strings.Count(b, "A")+strings.Count(b, "T")) / float64(len(b))
	pgcA := float64(strings.Count(a, "C")+strings.Count(a, "G")) / float64(len(a))
	pgcB := float64(strings.Count(b, "C")+strings.Count(b, "G")) / float64(len(b))

	//fmt.Println("a:", a, " b:", b, " pgcA:", pgcA, " pgcB:", pgcB)
	//fmt.Println("patA:", patA, " patB:", patB)
	atDiff := math.Abs(float64(patA - patB))
	gcDiff := math.Abs(float64(pgcA - pgcB))
	//fmt.Println("GCDIFF:=", gcDiff)
	//fmt.Println("ATDiff:=", atDiff)
	ret := (1 - completeRatio) + gcDiff + atDiff + EuclideanDistance(a, b, kmer)*(1-(0.25*(jaccard+float64(lcsseqlen)/float64(minL)+float64(lcsstrlen)/float64(minL)+((float64(minL)-float64(edist))/float64(minL)))))
	return ret
}

//K = RawDistance * (1-SIM)
